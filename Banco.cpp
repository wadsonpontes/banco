#include <iostream>
#include <string>

class ClientePJ {
	private:
	std::string razao_social;
	std::string cnpj;
	
	public:	
	std::string getRazaoSocial() {
		return this->razao_social;
	}
	void setRazaoSocial(std::string razao_social) {
		this->razao_social = razao_social;
	}
	std::string getCnpj() {
		return this->cnpj;
	}
	void setCnpj(std::string cnpj) {
		this->cnpj = cnpj;
	}
};

class Cliente {
	public:
	std::string nome;
	std::string cpf;
};

template <class T> class Fila {
	private:
	int size;
	int top;
	T* elements;

	public:
	Pilha(int s){
		this->size = s;
		this->top = -1;
		this->elements = new T[size];
    }

    void push(T element){
		if(top == (size -1)){
			cout << "Pilha cheia" << endl;
		}else{
			this->elements[++top] = element;
        }
    }
    void pop(){
        if(top == -1){
			cout << "Pilha vazia" << endl;
        }else{
			this->elements[top--];
        }
    }
    bool isEmpty(){
        if(top == -1){
			return true;
        }else{
			return false;
        }
    }
    T back(){
        return this->elements[top];
    }
};

class Agencia {
	public:
	std::string nome;
	int numeroDaAgencia;
};

class Conta {
	public:
	int numero;
	Cliente cliente;
	Agencia agencia;
	double saldo;
	double limite;
	static int quantidade_contas;

	Conta() {
		this->quantidade_contas += 1;
	}

	Conta(const Conta &conta) {
		this->quantidade_contas += 1;
	}

	~Conta() {
		this->quantidade_contas -= 1;
	}

	double saca(double valor) {
		if (valor > this->limite) {
			std::cout << "Valor ultrapassa o limite!" << std::endl;
		}
		else if (valor > this->saldo) {
			std::cout << "Valor ultrapassa o saldo!" << std::endl;
		}
		else {
			this->saldo -= valor;
			std::cout << "Sacado com sucesso!" << std::endl;
			return valor;
		}
		return 0;
	}
	void deposita(double valor) {
		if (valor > this->limite) {
			std::cout << "Valor ultrapassa o limite!" << std::endl;
		}
		else {
			this->saldo += valor;
			std::cout << "Depositado com sucesso!" << std::endl;
		}
	}

	void transfere(double valor, Conta &conta) {
		if (valor > this->limite) {
			std::cout << "Valor ultrapassa o limite!" << std::endl;
		}
		else if (valor > this->saldo) {
			std::cout << "Valor ultrapassa o saldo!" << std::endl;
		}
		else {
			this->saldo -= valor;
			conta.saldo += valor;
			std::cout << "Transferido com sucesso!" << std::endl;
		}
	}
};

int Conta::quantidade_contas;
